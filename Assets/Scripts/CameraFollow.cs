﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField] private Transform target;
	[SerializeField] private float camSpeed = 0.15f;
	[SerializeField] private float distance = 1;

	private void LateUpdate()
	{
		Vector3 targetPos = new Vector3(transform.position.x, transform.position.y, target.position.z + distance);
		Vector3 smoothPos = Vector3.Lerp(transform.position, targetPos, camSpeed);
		transform.position = smoothPos;
	}
}
