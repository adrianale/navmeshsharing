﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
	private float posX = 0;

	void Awake()
	{
		posX = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = new Vector3(posX + Mathf.PingPong(Time.time * 2, 1) + 2, transform.position.y, transform.position.z);
		transform.position = pos;
	}
}
