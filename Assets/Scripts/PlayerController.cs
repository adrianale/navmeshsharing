﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
	[SerializeField] Camera cam;
	[SerializeField] NavMeshAgent agent;

	[SerializeField] private bool isMouseDownFollow;
	
	// Update is called once per frame
	void Update ()
	{
		bool mouseClick = Input.GetMouseButtonDown(0) && !isMouseDownFollow ||
		                  Input.GetMouseButton(0) && isMouseDownFollow;
		if (mouseClick)
		{
			Ray ray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit))
			{
				agent.SetDestination(hit.point);
			}
		}
	}
}
