﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TileGenerator : MonoBehaviour
{
	public int count = 4;
	public GameObject tile;
	public Transform parent;
	public NavMeshSurface surface;

	// Use this for initialization
	void Start () {
		for (int i = 0; i < count; i++)
		{
			GameObject obj = Instantiate(tile, parent, true);
			obj.transform.position = new Vector3(0f, 0f, i * 5f);
		}
		
		surface.BuildNavMesh();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
