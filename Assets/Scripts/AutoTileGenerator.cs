﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AutoTileGenerator : MonoBehaviour
{
	public GameObject tilePrefab;
	public Transform parent;
	public NavMeshSurface surface;
	
	private Dictionary<int, GameObject> tiles = new Dictionary<int, GameObject>();

	// Use this for initialization
	void Start () {
//		surface.BuildNavMesh();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float posZ = transform.position.z + 5;
		int index = (int)(posZ / 5f);

		if (!tiles.ContainsKey(index))
		{
			GameObject obj = Instantiate(tilePrefab, parent, true);
			obj.transform.position = new Vector3(0f, 0f, index * 5f);
			
			tiles.Add(index, obj);
			surface.BuildNavMesh();
		}
	}
}
